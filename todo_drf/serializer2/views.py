import imp
from .models import Task1
from .seializers import TaskSerializer
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import generics
from rest_framework import viewsets
from django.shortcuts import get_object_or_404
from rest_framework.authentication import BasicAuthentication,SessionAuthentication
from rest_framework.permissions import IsAuthenticated,IsAdminUser,IsAuthenticatedOrReadOnly,DjangoModelPermissions,DjangoModelPermissionsOrAnonReadOnly
from .custompermission import Mypermission


#*************************************************APIVIEW********************************************************

class SnippetList(APIView):
    authentication_classes=[SessionAuthentication]
    permission_classes=[IsAuthenticatedOrReadOnly]
   
    def get(self, request, format=None):
        snippets = Task1.objects.all()
        serializer = TaskSerializer(snippets, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = TaskSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
 
class SnippetDetail(APIView):
    authentication_classes=[SessionAuthentication]
    permission_classes=[IsAuthenticatedOrReadOnly]
  
  
    def get_object(self, pk):
        try:
            return Task1.objects.get(pk=pk)
        except Task1.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = TaskSerializer(snippet)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = TaskSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)   
    
    
    
    

#**********************************************************GENERIC VIEW**************************************
class SnippetList1(generics.ListCreateAPIView):
        queryset = Task1.objects.all()
        serializer_class = TaskSerializer
        authentication_classes=[SessionAuthentication]
        authentication_permissions=[Mypermission]


class SnippetDetail1(generics.RetrieveUpdateDestroyAPIView):
    queryset = Task1.objects.all()
    serializer_class = TaskSerializer
    authentication_classes=[SessionAuthentication]
    authentication_permissions=[Mypermission]


# ListCreateAPIView
# RetrieveUpdateAPIView
# RetrieveDestroyAPIView
# RetrieveUpdateDestroyAPIView

#************************************************VIEWSET*******************************************
    
class UserViewSet(viewsets.ViewSet):
    authentication_classes=[SessionAuthentication]
    permission_classes=[IsAdminUser]
  
    def list(self, request):
        queryset = Task1.objects.all()
        serializer = TaskSerializer(queryset, many=True)
        return Response(serializer.data)
    

    def retrieve(self, request, pk=None):
        queryset = Task1.objects.all()
        user = get_object_or_404(queryset, pk=pk)
        serializer = TaskSerializer(user)
        return Response(serializer.data)
    
    
    
#****************************************************MODEL VIEWSET********************************************8
    
class UserViewSet1(viewsets.ModelViewSet):
   
    serializer_class = TaskSerializer
    queryset = Task1.objects.all()
    authentication_classes=[SessionAuthentication]
    permission_classes=[Mypermission]
    

    
#****************************************************READONLYMODELVIEWSET********************************************8
    
    
class AccountViewSet(viewsets.ReadOnlyModelViewSet):

    queryset = Task1.objects.all()
    serializer_class = TaskSerializer
    authentication_classes=[SessionAuthentication]
    authentication_permissions=[IsAuthenticated]



class UserViewSet2(viewsets.ModelViewSet):
       
    serializer_class = TaskSerializer
    queryset = Task1.objects.all()
    authentication_classes=[SessionAuthentication]
    permission_classes=[DjangoModelPermissions]
    
class UserViewSet3(viewsets.ModelViewSet):
       
    serializer_class = TaskSerializer
    queryset = Task1.objects.all()
    authentication_classes=[SessionAuthentication]
    permission_classes=[DjangoModelPermissionsOrAnonReadOnly]
    