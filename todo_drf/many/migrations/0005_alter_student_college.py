# Generated by Django 4.0.1 on 2022-01-25 10:54

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('many', '0004_alter_student_college'),
    ]

    operations = [
        migrations.AlterField(
            model_name='student',
            name='College',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='many.college'),
        ),
    ]
