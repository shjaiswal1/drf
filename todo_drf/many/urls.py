from rest_framework.routers import DefaultRouter
from .views import Studentviewset,Collegeviewset,Coursesviewset,Departmentviewset,Programmeviewset

router = DefaultRouter()
router.register(r'student', Studentviewset)
router.register(r'college', Collegeviewset)
router.register(r'course', Coursesviewset)
router.register(r'department', Departmentviewset)
router.register(r'programme',Programmeviewset)

urlpatterns = [
]
urlpatterns += router.urls