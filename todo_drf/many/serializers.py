from rest_framework import serializers
from .models import Department,College,Courses,Programme,Student

class StudentSerializer(serializers.ModelSerializer):
	Department = serializers.CharField(source='Department.name')
	college = serializers.CharField(source='College.name')
	class Meta:
		model = Student
		fields =['name','age','Department','Courses','college',]


  
class DepartmentSerializer(serializers.ModelSerializer):
	class Meta:
		model = Department
		fields ='__all__'
  

class CollegeSerializer(serializers.ModelSerializer):
	class Meta:
		model = College
		fields ='__all__'

class CoursesSerializer(serializers.ModelSerializer):
	class Meta:
		model = Courses
		fields ='__all__'
	
  
class ProgrammeSerializer(serializers.ModelSerializer):
	class Meta:
		model = Programme
		fields ='__all__'