from django.shortcuts import render
from .models import Student,Department,College,Courses,Programme
from .serializers import StudentSerializer,CollegeSerializer,CoursesSerializer,ProgrammeSerializer,DepartmentSerializer
from rest_framework import viewsets



class Studentviewset(viewsets.ModelViewSet):
   
    serializer_class = StudentSerializer
    queryset = Student.objects.all()
    
    

class Collegeviewset(viewsets.ModelViewSet):
       
    serializer_class = CollegeSerializer
    queryset = College.objects.all()
    
class Coursesviewset(viewsets.ModelViewSet):
       
    serializer_class = CoursesSerializer
    queryset = Courses.objects.all()
    
    
class Programmeviewset(viewsets.ModelViewSet):
       
    serializer_class = ProgrammeSerializer
    queryset = Programme.objects.all()
    
    
    
class Departmentviewset(viewsets.ModelViewSet):
       
    serializer_class = DepartmentSerializer
    queryset = Department.objects.all()