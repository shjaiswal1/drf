from django.db import models

# Create your models here.
from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class Task1(models.Model):
  title = models.CharField(max_length=200)
  completed = models.BooleanField(default=False, blank=True, null=True)

  def __str__(self):
    return self.title




class Programme(models.Model):
      name = models.CharField(max_length=200)
      duration=models.IntegerField(max_length=2)
      
      def __str__(self):
            return self.name



class Courses(models.Model):
      name = models.CharField(max_length=200)
      duration=models.IntegerField(max_length=2)
      
      def __str__(self):
            return self.name
          
class Department(models.Model):
      name = models.CharField(max_length=200)
      head=models.CharField(max_length=100)

      
      def __str__(self):
            return self.name 
          
class College(models.Model):
      user=models.ManyToManyField(User)
      name = models.CharField(max_length=200)
      location=models.CharField(max_length=100)
      Department=models.ManyToManyField(Department)
      Courses=models.ManyToManyField(Courses)
      Programme=models.ManyToManyField(Programme)
      
      def __str__(self):
            return self.name

          
class Student(models.Model):
      user=models.ManyToManyField(User)
      name = models.CharField(max_length=200)
      age=models.IntegerField(max_length=3)
      Courses = models.ForeignKey(to=Courses,on_delete=models.CASCADE,null=True,blank=True)
      Programme=models.ForeignKey(to=Programme,on_delete=models.CASCADE,null=True,blank=True)
      College=models.ForeignKey(to=College,on_delete=models.CASCADE,null=True,blank=True)
      Department=models.ForeignKey(to=Department,on_delete=models.CASCADE,null=True,blank=True)
      
      def __str__(self):
           return self.name
     
     
      
      
  