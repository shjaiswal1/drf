from django.contrib import admin

# Register your models here.
# Register your models here.
from .models import Task1,Student,Courses,College,Department,Programme

admin.site.register(Task1)
admin.site.register(Student)
admin.site.register(Courses)
admin.site.register(College)
admin.site.register(Department)
admin.site.register(Programme)