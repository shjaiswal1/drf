
from django.http import JsonResponse, request
from django.shortcuts import render
from .forms import Employeedetails
from .models import details
from django import views
from django.http import JsonResponse
# from django.core import serializers
from django.core.paginator import Paginator
from .serializers import detailSerializer
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.forms.models import model_to_dict
from django.core import serializers
import json
from django.shortcuts import redirect




class DataTableView(views.View):

    def get(self,request,pk=None):
        employees = details.objects.all().order_by('-id')
        form = Employeedetails()


        return render(request, template_name='ajaxCrudApp/datatable_static.html', context={'employees': employees,
                                                                                            'form': form})  

@method_decorator(csrf_exempt, name='dispatch')
class CreateView(views.View):
    def post(self, request,pk=None):
        form = Employeedetails(request.POST)
        
        if form.is_valid():
            print("IT IS VALID")
            emp_id = request.POST['employee_id']
            name = request.POST['employee_name']
            age = request.POST['employee_age']
            location = request.POST['employee_location']
            print(f"ID {emp_id}")
            if(emp_id == ""):
                employee = details.objects.create(employee_name=name,
                                        employee_age=age, 
                                        employee_location=location
                                        )
            else:
                print("IT IS NOT VALID")
                employee = details(
                                        id = emp_id,
                                        employee_name=name,
                                        employee_age=age, 
                                        employee_location=location,
                                        )
            employee.save()
           
           
            serializer = detailSerializer(employee)
            employees = details.objects.values().order_by('-id')
            employees_list = list(employees)
            
            return JsonResponse({'status': 1,'employees_data': serializer.data, 'employees': employees_list})
        else:
            return JsonResponse({'status': 0})

class DeleteView(views.View):
    def post(self, request,pk=None):
        id = request.POST['sid']
        # print(id)
        employee = details.objects.get(pk=id)
        employee.delete()
        
        employees_data = list(details.objects.values())
        return JsonResponse({'status': 1})

class UpdateView(views.View):
    def post(self, request,pk=None):
        id = request.POST['sid']
        print(id)
        employee = details.objects.get(pk=id)
        employee_data = {
            "id": employee.id,
            "employee_name": employee.employee_name,
            "employee_age": employee.employee_age,
            "employee_location": employee.employee_location
           

        }
        return JsonResponse(employee_data)
        


 




    
  